/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.treehouse.rest;

import com.treehouse.business.mail.PasswordRequestionMailer;
import com.treehouse.persistence.Account;
import com.treehouse.persistence.Account_;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

/**
 *
 * @author rudolfschmidt.com
 */
@Stateless
@Path("passwordRequestion")
public class PasswordRequestion {

    @PersistenceContext
    private EntityManager em;
    @Inject
    private PasswordRequestionMailer mailer;

    @POST
    @Path("newPassword")
    public Response setNewPassword(NewPasswordData data) {
        final CriteriaBuilder cb = em.getCriteriaBuilder();
        final CriteriaQuery<Account> cq = cb.createQuery(Account.class);
        cq.where(cb.equal(cq.from(Account.class).get(Account_.passwordRequestionHash), data.hash));
        final Account account = em.createQuery(cq).getSingleResult();
        account.setPasswordRequestionHash(null);
        account.setPassword(data.password);
        account.setModified(new Date());
        return Response.status(Response.Status.ACCEPTED).build();
    }

    @GET
    @Path("verification/{hash:[\\w-]+}")
    public Response verification(@PathParam("hash") String hash) {
        final CriteriaBuilder cb = em.getCriteriaBuilder();
        final CriteriaQuery<Account> cq = cb.createQuery(Account.class);
        cq.where(cb.equal(cq.from(Account.class).get(Account_.passwordRequestionHash), hash));
        try {
            em.createQuery(cq).getSingleResult();
        } catch (NoResultException ex) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        return Response.status(Response.Status.ACCEPTED).build();
    }

    @POST
    public Response passwordRequestion(PasswordRequestionData data) {
        //find account with given email address
        final CriteriaBuilder cb = em.getCriteriaBuilder();
        final CriteriaQuery<Account> cq = cb.createQuery(Account.class);
        final Root<Account> root = cq.from(Account.class);
        cq.where(cb.equal(root.get(Account_.email), data.email));
        final Account account;
        try {
            account = em.createQuery(cq).getSingleResult();
        } catch (NoResultException ex) {
            return Response.status(Response.Status.FORBIDDEN).entity(data.email).build();
        }
        //create unique password requestion hash and set hash to account
        final CriteriaQuery<String> hq = cb.createQuery(String.class);
        hq.select(hq.from(Account.class).get(Account_.passwordRequestionHash));
        final List<String> hashes = em.createQuery(hq).getResultList();
        String hash = null;
        boolean unique = false;
        while (!unique) {
            hash = UUID.randomUUID().toString();
            if (!hashes.contains(hash)) {
                unique = true;
            }
        }
        account.setPasswordRequestionHash(hash);
        //send email message with hash link for password reentering
        try {
            mailer.mail(account.getFirstName(), data.email, hash);
        } catch (MessagingException ex) {
            throw new RuntimeException("Email could not be sent", ex);
        }
        //return ok
        return Response.status(Response.Status.ACCEPTED).build();
    }

    public static class PasswordRequestionData {

        public String email;
    }

    public static class NewPasswordData {

        public String hash;
        public String password;
    }
}
