/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.treehouse.rest;

import com.treehouse.persistence.Account;
import com.treehouse.persistence.Account_;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

/**
 *
 * @author rudolfschmidt.com
 */
@Path("login")
public class Login {

    @PersistenceContext
    private EntityManager em;

    @POST
    public Response login(Data data) {
        final CriteriaBuilder cb = em.getCriteriaBuilder();
        final CriteriaQuery<Account> c = cb.createQuery(Account.class);
        final Root<Account> root = c.from(Account.class);
        c.where(cb.equal(root.get(Account_.email), data.email),
                cb.equal(root.get(Account_.password), data.password));
        try {
            em.createQuery(c).getSingleResult();
        } catch (NoResultException ex) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        return Response.status(Response.Status.ACCEPTED).build();
    }

    public static class Data {

        public String email;
        public String password;
    }
}
