/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.treehouse.rest;

import com.treehouse.business.mail.RegistrationMailer;
import com.treehouse.persistence.Account;
import com.treehouse.persistence.Account_;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

/**
 *
 * @author rudolfschmidt.com
 */
@Stateless
@Path("registration")
public class Registration {

    @PersistenceContext
    private EntityManager em;
    @Inject
    private RegistrationMailer mailer;

    @GET
    @Path("verification/{hash:[\\w-]+}")
    public Response verification(@PathParam("hash") String hash) {
        final CriteriaBuilder cb = em.getCriteriaBuilder();
        final CriteriaQuery<Account> cq = cb.createQuery(Account.class);
        cq.where(cb.equal(cq.from(Account.class).get(Account_.accountVerificationHash), hash));
        final Account account;
        try {
            account = em.createQuery(cq).getSingleResult();
        } catch (NoResultException ex) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        account.setAccountVerified(true);
        account.setAccountVerificationHash(null);
        account.setModified(new Date());
        return Response.status(Response.Status.ACCEPTED).build();
    }

    @POST
    public Response registration(Data data) {
        if (data.firstName == null || data.secondName == null || data.email == null | data.password == null) {
            return Response.status(Response.Status.UNSUPPORTED_MEDIA_TYPE).build();
        }
        final CriteriaBuilder cb = em.getCriteriaBuilder();
        final CriteriaQuery<Account> cq = cb.createQuery(Account.class);
        cq.where(cb.equal(cq.from(Account.class).get(Account_.email), data.email));
        //check used email
        if (!em.createQuery(cq).getResultList().isEmpty()) {
            return Response.status(Response.Status.FORBIDDEN).entity(data.email).build();
        }
        //create unique verification hash
        final CriteriaQuery<String> b = cb.createQuery(String.class);
        b.select(b.from(Account.class).get(Account_.accountVerificationHash));
        final List<String> resultList = em.createQuery(b).getResultList();
        String verificationHash = null;
        boolean unique = false;
        while (!unique) {
            verificationHash = UUID.randomUUID().toString();
            if (!resultList.contains(verificationHash)) {
                unique = true;
            }
        }
        //create new user account
        final Account account = em.merge(new Account());
        account.setFirstName(data.firstName);
        account.setSecondName(data.secondName);
        account.setEmail(data.email);
        account.setPassword(data.password);
        account.setAccountVerificationHash(verificationHash);
        account.setCreated(new Date());
        account.setModified(new Date());
        try {
            mailer.mail(data.firstName, data.email, verificationHash);
        } catch (MessagingException ex) {
            throw new RuntimeException("Email could not be sent", ex);
        }
        return Response.status(Response.Status.CREATED).build();
    }

    public static class Data {

        public String firstName;
        public String secondName;
        public String email;
        public String password;
    }
}
