/*
 * Copyright (C) 2013 Treehouse
 */
package com.treehouse.persistence;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

/**
 *
 * @author Rudolf Schmidt <mail@rudolfschmidt.com>
 */
@Entity
public class Account implements Serializable {

    @Id
    @GeneratedValue
    private long id;
    private String firstName;
    private String secondName;
    private String email;
    private String password;
    private boolean requestDeletion;
    private boolean accountVerified;
    private String accountVerificationHash;
    private String passwordRequestionHash;

    @OneToMany(mappedBy = "owner")
    private List<Project> ownedProjects;
    @OneToMany(mappedBy = "owner")
    private List<Topic> ownedTopics;
    @OneToMany(mappedBy = "owner")
    private List<Contribution> ownedContributions;

    @OneToMany(mappedBy = "projectLeader")
    private List<ProjectLeading> projectLeadings;
    @OneToMany(mappedBy = "projectMember")
    private List<ProjectMembering> projectMemberings;
    @OneToMany(mappedBy = "topicLeader")
    private List<TopicLeading> topicLeadings;
    @OneToMany(mappedBy = "topicMember")
    private List<TopicMembering> topicMemberings;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date created;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date modified;

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Account other = (Account) obj;
        return this.id == other.id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isRequestDeletion() {
        return requestDeletion;
    }

    public void setRequestDeletion(boolean requestDeletion) {
        this.requestDeletion = requestDeletion;
    }

    public boolean isAccountVerified() {
        return accountVerified;
    }

    public void setAccountVerified(boolean accountVerified) {
        this.accountVerified = accountVerified;
    }

    public String getAccountVerificationHash() {
        return accountVerificationHash;
    }

    public void setAccountVerificationHash(String accountVerificationHash) {
        this.accountVerificationHash = accountVerificationHash;
    }

    public String getPasswordRequestionHash() {
        return passwordRequestionHash;
    }

    public void setPasswordRequestionHash(String passwordRequestionHash) {
        this.passwordRequestionHash = passwordRequestionHash;
    }

    public List<Project> getOwnedProjects() {
        return ownedProjects;
    }

    public void setOwnedProjects(List<Project> ownedProjects) {
        this.ownedProjects = ownedProjects;
    }

    public List<Topic> getOwnedTopics() {
        return ownedTopics;
    }

    public void setOwnedTopics(List<Topic> ownedTopics) {
        this.ownedTopics = ownedTopics;
    }

    public List<Contribution> getOwnedContributions() {
        return ownedContributions;
    }

    public void setOwnedContributions(List<Contribution> ownedContributions) {
        this.ownedContributions = ownedContributions;
    }

    public List<ProjectLeading> getProjectLeadings() {
        return projectLeadings;
    }

    public void setProjectLeadings(List<ProjectLeading> projectLeadings) {
        this.projectLeadings = projectLeadings;
    }

    public List<ProjectMembering> getProjectMemberings() {
        return projectMemberings;
    }

    public void setProjectMemberings(List<ProjectMembering> projectMemberings) {
        this.projectMemberings = projectMemberings;
    }

    public List<TopicLeading> getTopicLeadings() {
        return topicLeadings;
    }

    public void setTopicLeadings(List<TopicLeading> topicLeadings) {
        this.topicLeadings = topicLeadings;
    }

    public List<TopicMembering> getTopicMemberings() {
        return topicMemberings;
    }

    public void setTopicMemberings(List<TopicMembering> topicMemberings) {
        this.topicMemberings = topicMemberings;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

}
