/*
 * Copyright (C) 2014 Treehouse
 */
package com.treehouse.business.mail;

import java.io.InputStream;
import java.util.Scanner;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.MessagingException;

/**
 *
 * @author rudolfschmidt.com
 */
@Stateless
public class PasswordRequestionMailer {

    @Inject
    Mailer mailer;

    public void mail(String name, String address, String hash) throws MessagingException {
        final InputStream stream = getClass().getClassLoader().getResourceAsStream("passwordRequestMail.txt");
        final String content = new Scanner(stream, "UTF-8").useDelimiter("\\A").next();
        final String link = "http://app.treehouse-project.com/#/passwordRequestion/verification/" + hash;
        final String text = String.format(content, name, address, link);
        mailer.mail(address, "User password request", text);
    }
}
