var app = angular.module("app.routing", ["ui.router"]);
app.config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/login");
    $stateProvider.state("stripe", {
        templateUrl: "views/stripe.html",
    });
    $stateProvider.state("stripe.login", {
        url: "/login",
        templateUrl: "views/login.html",
        controller: "LoginController"
    });
    $stateProvider.state("stripe.registration", {
        url: "/registration",
        templateUrl: "views/registration.html",
        controller: "RegistrationController"
    });
    $stateProvider.state("stripe.registrationSuccess", {
        templateUrl: "views/registrationSuccess.html",
        controller: "RegistrationSuccessController"
    });
    $stateProvider.state("stripe.registrationVerification", {
        url: "/registration/verification/:hash",
        templateUrl: "views/registrationVerification.html",
        controller: "RegistrationVerificationController"
    });
    $stateProvider.state("stripe.passwordRequestion", {
        url: "/passwordRequestion",
        templateUrl: "views/passwordRequestion.html",
        controller: "PasswordRequestionController"
    });
    $stateProvider.state("stripe.passwordRequestionSuccess", {
        templateUrl: "views/passwordRequestionSuccess.html",
    });
    $stateProvider.state("stripe.passwordRequestionVerification", {
        url: "/passwordRequestion/verification/:hash",
        templateUrl: "views/passwordRequestionVerification.html",
        controller: "PasswordRequestionVerificationController"
    });
    $stateProvider.state("stripe.passwordRequestionPasswordChangedSuccess", {
        templateUrl: "views/passwordRequestionPasswordChangedSuccess.html",
    });
    $stateProvider.state("projects", {
        url: "/projects",
        templateUrl: "views/projects.html",
        controller: "ProjectsController"
    });
});
