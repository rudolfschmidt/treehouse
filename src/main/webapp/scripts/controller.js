var app = angular.module("app.controller", []);
app.controller("LoginController", function($scope, $http, $state) {
    $scope.submit = function() {
        $http.post("resources/login", $scope.data)
                .success(function() {
                    $state.transitionTo("projects");
                })
                .error(function(data, status) {
                    if (status === 403) {
                        $scope.forbidden = true;
                    } else {
                        $scope.error = true;
                    }
                });
    };
});
app.controller("RegistrationController", function($scope, $http, $state) {
    $scope.submit = function() {
        $http.post("resources/registration", $scope.data)
                .success(function() {
                    $state.transitionTo("stripe.registrationSuccess");
                })
                .error(function(data, status) {
                    if (status === 403) {
                        $scope.forbidden = {email: data};
                    } else {
                        $scope.error = true;
                    }
                });
    };
});
app.controller("RegistrationSuccessController", function($state, $timeout) {
    $timeout(function() {
        $state.transitionTo("projects");
    }, 5000);
});
app.controller("RegistrationVerificationController", function($scope, $http, $stateParams) {
    $http.get("resources/registration/verification/" + $stateParams.hash)
            .success(function(data) {
                $scope.success = true;
            })
            .error(function(data, status) {
                if (status === 403) {
                    $scope.forbidden = true;
                } else {
                    $scope.error = true;
                }
            });
});
app.controller("PasswordRequestionController", function($scope, $http, $state) {
    $scope.submit = function() {
        $http.post("resources/passwordRequestion", $scope.data)
                .success(function() {
                    $state.transitionTo("stripe.passwordRequestionSuccess");
                })
                .error(function(data, status) {
                    if (status === 403) {
                        $scope.forbidden = {email: data};
                    } else {
                        $scope.error = true;
                    }
                });
    };
});
app.controller("PasswordRequestionVerificationController", function($scope, $http, $stateParams, $state) {
    $scope.showFirstMsg = function() {
        return $scope.form.first.$invalid && $scope.form.first.$dirty;
    };
    $scope.showSecondMsg = function() {
        return $scope.form.second.$invalid && $scope.form.second.$dirty;
    };
    $scope.showMatchingMsg = function() {
        return $scope.form.first.$valid && $scope.form.second.$valid && $scope.first !== $scope.second;
    };
    $scope.showFirstValid = function() {
        return $scope.form.first.$valid;
    };
    $scope.showSecondValid = function() {
        return $scope.form.second.$valid && $scope.first === $scope.second;
    };
    $scope.showFirstInvalid = function() {
        return $scope.form.first.$invalid && $scope.form.first.$dirty;
    };
    $scope.showSecondInvalid = function() {
        return ($scope.form.second.$invalid || $scope.first !== $scope.second) && $scope.form.second.$dirty;
    };
    $http.get("resources/passwordRequestion/verification/" + $stateParams.hash)
            .success(function() {
                $scope.success = true;
            })
            .error(function(data, status) {
                if (status === 403) {
                    $scope.forbidden = true;
                } else {
                    $scope.error = true;
                }
            });
    $scope.submit = function() {
        $http.post("resources/passwordRequestion/newPassword", {password: $scope.first, hash: $stateParams.hash})
                .success(function() {
                    $state.transitionTo("stripe.passwordRequestionPasswordChangedSuccess");
                })
                .error(function(data, status) {
                    if (status === 403) {
                        $scope.forbidden = {email: data};
                    } else {
                        $scope.error = true;
                    }
                });
    };
});
app.controller("ProjectsController", function() {

});